import React from 'react'
import { NavDropdown, Form, FormControl, Button,Nav, Navbar ,Container} from 'react-bootstrap'
import {NavLink} from 'react-router-dom'

function MenuBar() {



    return (
        <div>
            <Navbar bg="primary" className="navbar" expand="lg">
                <Container>
                    <Navbar.Brand href="#">AMS Redux</Navbar.Brand>
                    <Navbar.Toggle aria-controls="navbarScroll" />
                    <Navbar.Collapse id="navbarScroll">
                        <Nav
                            className="mr-auto my-2 my-lg-0"
                            style={{ maxHeight: "100px" }}
                            navbarScroll
                        >
                            <Nav.Link as={NavLink} to='/'>Home</Nav.Link>
                            <Nav.Link as={NavLink} to='/article'>Article</Nav.Link>
                            <Nav.Link as={NavLink} to='/author'>Author</Nav.Link>
                            <Nav.Link as={NavLink} to='/category'>Category</Nav.Link>
                            <NavDropdown title="Language" id="navbarScrollingDropdown">
                                <NavDropdown.Item >Khmer</NavDropdown.Item>
                                <NavDropdown.Item o>
                                    English
                                </NavDropdown.Item>
                            </NavDropdown>
                        </Nav>
                        <Form className="d-flex">
                            <FormControl
                                type="search"
                                placeholder="Search"
                                className="mr-2"
                                aria-label="Search"
                            />
                            <Button variant="outline-light">Search</Button>
                        </Form>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </div>
    )
}

export default MenuBar
