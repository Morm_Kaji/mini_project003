import React from 'react'
import { useParams } from 'react-router'
import { useDispatch, useSelector } from 'react-redux'
import { useEffect } from 'react';
import { fetchArticle_id } from '../redux/actions/ArticleFetch';
import { Container, Row, Col } from 'react-bootstrap'


function ShowArticle() {
    let { id } = useParams();
    const dispatch = useDispatch();
    const state = useSelector(state => state.state)
    useEffect(() => {
        dispatch(fetchArticle_id(id));
    }, [])
    return (
        <Container>
            {
                state ? (
                    <>
                        <Row>
                            <Col md={8}>
                                <h1 className="my-2">{state.title}</h1>
                                <img
                                    width="500"
                                    height="100%"
                                    alt="..."
                                    src={
                                        state.image
                                            ? state.image
                                            : "https://steamuserimages-a.akamaihd.net/ugc/644374753312153149/5E6920279A80734500AA85DC7B12399091D8689D/"
                                    }
                                />
                                <p>{state.description}</p>
                            </Col>
                            <Col md={4}>

                            </Col>
                        </Row>
                    </>
                ) : (
                    <h2>.... Loading .....</h2>
                )
        }
        </Container>
    )
}

export default ShowArticle
