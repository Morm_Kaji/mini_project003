import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css'
import { Container } from 'react-bootstrap'
import { Switch, Route, BrowserRouter } from 'react-router-dom'
import MenuBar from './components/MenuBar';
import Article from './views/Article';
import Author from './views/Authors';
import Category from './views/Category';
import Home from './views/Home';
import ShowArticle from './components/ShowArticle';



function App() {
  return (
    <div>
      <Container>
        <BrowserRouter>
          <MenuBar />
          <Switch>

            <Route exact path='/' component={Home} />
            <Route path='/article' component={Article} />
            <Route path='/author' component={Author} />
            <Route path='/category' component={Category} />
            <Route path='/view/:id' component={ShowArticle} />
            <Route path='/update/article/:id' component={Article} />
          </Switch>
        </BrowserRouter>
      </Container>
    </div>
  );
}

export default App;
