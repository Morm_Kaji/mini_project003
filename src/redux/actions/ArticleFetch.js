import { api } from "../../utils/api"


export const fetchArticle = () => async dp =>{
    let response = await api.get('/article');
    return dp({
        type: "FETCH_ARTICLE",
        payload: response.data.data
    })
}

export const deleteArticle = (id) => async dp =>{
    let response = await api.delete('/article/'+id);

    return dp({
        type: "DELETE_ARTICLE",
        payload: response.data.data
    })
}

export const postArticle = (item) => async dp =>{
    let response = await api.post('/article',item)

    return dp({
        type: "POST_ARTICLE",
        payload: response.data.message
    })
}

export const uploadImg = (img) => async dp => {
    let image = new Image();
   

    image.append('image',img);
    let response = await api.post('/images',image);
    return dp({
        type:"UPLOAD_ARTICLE",
        payload: response.data.url
    })
}

export const updateArticle = (id, artilce) => async dp =>{
    let response = await api.patch('/articles/'+id, artilce);

    return dp({
        type: "UPDATE_ARTICLE",
        payload: response.data.message
    })
}

export const fetchArticle_id=(id) => async dp =>{

    let response = await api.get('articles/'+id)
    return dp({
        type: "FETCH_ID_ARTICLE",
        payload: response.data.message
    })
}