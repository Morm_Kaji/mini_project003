import { api } from "../../utils/api"

export const fetchAuthor =() => async dp=>{
    let response = await api.get('/author')
    return dp({
        type: "FETCH_AUTHOR",
        payload: response.data.data
    })
}

export const postAuthor = (person) => async dp =>{
    let response = await api.post('/author', person);
    return dp({
        type: "POST_AUTHOR",
        payload: response.data.message
    })
}

export const deleteAuthor = (id) => async dp =>{
    let response = await api.delete('/author'+id);
    return dp({
        type: "DELETE_AUTHOR",
        payload: response.data.data
    })
}

export const updatAuthor =(id, person) => async dp =>{
    let response = await api.put('/author/'+id,person);
    return dp({
        type:"UPDATE_AUTHOR",
        payload:response.data.message
    })
}