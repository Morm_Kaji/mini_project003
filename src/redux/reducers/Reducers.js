import { combineReducers } from "redux";
import ArticleReducer from "./ArticleReducer";
import CategoryReducer from "./CategoryReducer";
import AuthorReducer from "./AuthorReducer";

const reducers = combineReducers({
    ArticleReducer,
    AuthorReducer,
    CategoryReducer
});

export default reducers;