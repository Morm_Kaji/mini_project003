
const initialState = {
    articles: [],
    view: [],
}

export default (state = initialState, { type, payload }) => {
    switch (type) {

        case "FETCH_AUTHOR":
            return { ...state, articles: payload }
        case "POST_ARTICLE":
            return { ...state, payload }
        case "DELETE_ARTICLE":
            return { ...state, articles: state.articles.filter(item => item._id !== payload._id) }
        case "UPLOAD_ARTICLE":
            return { ...state, payload }
        case "UPDATE_ARTICLE":
            return { ...state, view: payload }
        case "POST_ARTICLE":
            return { ...state, payload }
        case "FETCH_ID_ARTICLE":
            return { ...state, payload }

        default:
            return state
    }
}