import React, { useEffect, useState } from 'react'
import { Container, Row, Col, Button, Form } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { bindActionCreators } from "redux";
import { useParams } from "react-router";
import { fetchArticle_id, postArticle, updateArticle, uploadImg } from "../redux/actions/ArticleFetch"
import { fetchAuthor } from '../redux/actions/AuthorFetch'


function Article() {

    const [title, setTitle] = useState('')
    const [description, setDscription] = useState('')
    const [img, setImg] = useState('https://cdn.dribbble.com/users/310943/screenshots/2792692/empty-state-illustrations.gif')
    const [file, setFile] = useState(null)
    const [author, setAuthor] = useState([])
    const [author_id, setAuthor_id] = useState(null)

    const dispatch = useDispatch()
    const state = useSelector((state) => state.AuthorReducer.authors)
    const post = bindActionCreators(postArticle, dispatch)
    const upload = bindActionCreators(uploadImg, dispatch)
    const updateArticles = bindActionCreators(updateArticle, dispatch)

    const { id } = useParams()

    useEffect(() => {
        if (id) {
            dispatch(fetchArticle_id(id)).then(article => {
                setTitle(article.payload.title)
                setDscription(article.payload.description)
                setImg(article.payload.img)
            });
        }
        dispatch(fetchAuthor()).then(author => {
            setAuthor(author)
            setAuthor_id(author[0])
        });

    }, [])

    const add = async (e) => {
        e.preventDefault()
        let article = {
            title, description, author: author_id
        }

        if (file) {
            let url = await upload(file)
            article.image = url.payload
        }
        post(article)
    }

    const update = async (e) => {
        e.preventDefault()
        let article = {
            title, description, author: author_id
        }

        if (file) {
            let url = await upload(file)
            article.image = url.payload
        }
        updateArticles(id, article)
    }

    return (
        <Container className="my-4">
            <h1 className="my-2">{id ? "Update Article" : "Add Article"}</h1>
            <Row>
                <Col md={8}>
                    <Form>
                        <Form.Group controlId="title">
                            <Form.Label>Title</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Title"
                                value={title}
                                onChange={(e) => setTitle(e.target.value)}
                            />
                            <Form.Text className="text-muted"></Form.Text>
                        </Form.Group>

                        <Form.Group controlId="description">
                            <Form.Label>Author</Form.Label>
                            <Form.Control
                                as="select"
                                aria-label="Choose Author"
                                onChange={(e) => setAuthor_id(e.target.value)}
                            >
                                {state.map((author) => (
                                    <option
                                        key={author._id}
                                        value={author._id}
                                        selected={author._id === author_id}
                                    >
                                        {author.name}
                                    </option>
                                ))}
                            </Form.Control>
                        </Form.Group>

                        <Form.Group controlId="description">
                            <Form.Label>Description</Form.Label>
                            <Form.Control
                                as="textarea"
                                rows={4}
                                placeholder="Description"
                                value={description}
                                onChange={(e) => setDscription(e.target.value)}
                            />
                        </Form.Group>
                        <Button
                            variant="primary"
                            type="submit"
                            onClick={id ? update : add}
                        >
                            {id ? "Save" : "Submit"}
                        </Button>
                    </Form>
                </Col>
                <Col md={4}>
                    <img className="w-100" src={img} alt="default pic" />
                    <Form>
                        <Form.Group>
                            <Form.File
                                id="img"
                                label="Choose Image"
                                onChange={(e) => {
                                    let url = URL.createObjectURL(e.target.files[0]);
                                    setFile(e.target.files[0]);
                                    setImg(url);
                                }}
                            />
                        </Form.Group>
                    </Form>
                </Col>
            </Row>
        </Container>
    )
}

export default Article
