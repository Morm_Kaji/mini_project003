import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Container, Row, Col, Card, Button } from "react-bootstrap";
import { bindActionCreators } from "redux";
import { useHistory } from "react-router";
import {deleteArticle, fetchArticle} from "../redux/actions/ArticleFetch"
import {fetchCategory} from "../redux/actions/CategoryFetch"

function Home() {

    const dispatch = useDispatch();
    const onDelete = bindActionCreators(deleteArticle, dispatch);
    const article = useSelector((state) => state.ArticleReducer);
    const category = useSelector((state) => state.CategoryReducer.categories);

    const history = useHistory();


    useEffect(() => {
        dispatch(fetchArticle())
        dispatch(fetchCategory())
    }, []);


    return (
        <Container className="my-4">
                <Row>
                    <Col md="3">
                        <Card>
                            <Card.Img variant="top"
                                style={{ objectFit: "cover", height: "250px" }} src="https://cdn.dribbble.com/users/310943/screenshots/2792692/empty-state-illustrations.gif" />
                            <Card.Body>
                                <Card.Title>Please Login</Card.Title>
                                <Button variant="primary">Login Demo</Button>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col md="9">
                        <h1>Category</h1>
                        <Row className="my-3">
                            {category.map((item, index) => (
                                <Button key={index} className="mx-1" variant="outline-secondary" size="sm">{item.name}</Button>
                            ))}
                        </Row>
                        <Row className="my-3">
                            {article.articles.map((item, index) => (
                                <Col key={index} md="4">
                                    <Card>
                                        <Card.Img variant="top" style={{ objectFit: "cover", height: "150px" }} src={item.image} />
                                        <Card.Body>
                                            <Card.Title>{item.title}</Card.Title>
                                            <Card.Text className="text-line-3">{item.description}</Card.Text>
                                            <Button variant="primary" size='sm' onClick={() => history.push('/view/' + item._id)}>Read</Button>{" "}
                                            <Button
                                                variant="warning"
                                                size='sm'
                                                onClick={() => {
                                                    history.push('/update/article/' + item._id)
                                                }}
                                            >Edit</Button>{" "}
                                            <Button variant="danger" size='sm' onClick={() => onDelete(item._id)}>Delete</Button>
                                        </Card.Body>
                                    </Card>
                                </Col>
                            ))}
                        </Row>
                    </Col>
                </Row>
        </Container>
    )
}

export default Home
