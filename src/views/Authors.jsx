import React, { useEffect, useState } from "react";
import { Container, Form, Row, Col, Button, Table } from "react-bootstrap";
import { bindActionCreators } from "redux";
import { useDispatch, useSelector } from "react-redux";
import {deleteAuthor, fetchAuthor, postAuthor, updatAuthor} from "../redux/actions/AuthorFetch"
import {uploadImg} from "../redux/actions/ArticleFetch"


function Authors() {

    const [imageURL, setImageURL] = useState(
        "https://designshack.net/wp-content/uploads/placeholder-image.png"
      );
      const [img, setimg] = useState(null);
      const [name, setName] = useState(null);
      const [email, setEmail] = useState(null);
      const [isUpdate, setIsUpdate] = useState(null);

    
      const dispatch = useDispatch();
      const deleteAuthors = bindActionCreators(deleteAuthor, dispatch);
      const uploadImgs = bindActionCreators(uploadImg, dispatch);
      const post = bindActionCreators(postAuthor, dispatch);
      const updateAuthors= bindActionCreators(updatAuthor, dispatch);
      const state = useSelector((state) => state.AuthorReducer.authors);
    
      useEffect(() => {
        dispatch(fetchAuthor());
      }, []);
    
      const add = async (e) => {
        e.preventDefault();
        let authors = { name, email };
    
        if (img) {
          let url = await uploadImgs(img);
          authors.image = url.payload;
        }
        post(authors)
        setimg(null);
      };
    
      const update = (id, name, email, image) => {
        setIsUpdate(id)
        setName(name)
        setEmail(email)
        document.getElementById("name").value = name;
        document.getElementById("email").value = email;
        document.getElementById("image").src = image;
      };
    
      const onUpdate = async (e) => {
        e.preventDefault();
        let authors = { name, email };
    
        if (img) {
            let url = await uploadImg(img);
            authors.image = url.payload;
          }
    
        updateAuthors(isUpdate, authors)
        setIsUpdate(null);
        setimg(null);
      };

    return (
        <Container>
            <h1>Author</h1>
            <Row>
                <Col md={8}>
                    <Form>
                        <Form.Group>
                            <Form.Label>Auth Name</Form.Label>
                            <Form.Control
                                id="name"
                                type="text"
                                placeholder="Author Name"
                                onChange={(e) => setName(e.target.value)}
                            />
                            <Form.Text className="text-muted"></Form.Text>
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Email</Form.Label>
                            <Form.Control
                                id="email"
                                type="text"
                                placeholder="Email"
                                onChange={(e) => setEmail(e.target.value)}
                            ></Form.Control>
                        </Form.Group>
                        <Button
                            variant="primary"
                            type="submit"
                            onClick={isUpdate !== null ? onUpdate : add}
                        >
                            {isUpdate !== null ? "Save" : "Add"}
                        </Button>
                    </Form>
                </Col>
                <Col md={4}>
                    <img id="image" className="w-75" alt="..." src={imageURL} />
                    <Form>
                        <Form.Group>
                            <Form.File
                                id="img"
                                label="Choose Image"
                                onChange={(e) => {
                                    let url = URL.createObjectURL(e.target.files[0]);
                                    setimg(e.target.files[0]);
                                    setImageURL(url);
                                }}
                            />
                        </Form.Group>
                    </Form>
                </Col>
            </Row>

            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Image</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {state.map((author, index) => (
                        <tr key={index}>
                            <td>{author._id}</td>
                            <td>{author.name}</td>
                            <td>{author.email}</td>
                            <td>
                                <img src={author.image} alt="" width="auto" height={100} />
                            </td>
                            <td>
                                <Button
                                    size="sm"
                                    variant="warning"
                                    onClick={() =>
                                        update(author._id, author.name, author.email, author.image)
                                    }
                                >
                                    Edit
                                </Button>{" "}
                                <Button
                                    size="sm"
                                    variant="danger"
                                    onClick={() => deleteAuthors(author._id)}
                                >
                                    Delete
                                </Button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </Table>
        </Container>
    )
}

export default Authors
